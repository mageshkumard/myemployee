class Employee < ApplicationRecord
	validates :name, presence: true,
						 length: {minimum: 2}
	validates :age, :designation,:gender, presence: true
	validates_numericality_of :age, :message=>'should be in number.'
end
