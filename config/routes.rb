Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'employees#index', as: 'home'

  #get 'employees#new', as: 'new'
    
  resources :employees
end
